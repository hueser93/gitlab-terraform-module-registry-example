variable "username" {
  description = "The username who is greeted by the Terraform module."
  type        = string
  default     = "World"
}

